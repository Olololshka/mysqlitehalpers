/* Add your header comment here */
#include <sqlite3ext.h> /* Do not use <sqlite3.h>! */
#include <stddef.h>
#include <regex.h>
#include <string.h>
SQLITE_EXTENSION_INIT1

static regex_t tryParceAS;

void binary_INT2FLOAT(sqlite3_context* ctx, int argc, sqlite3_value* argv[]) {
    union {
        float f;
        unsigned char uc[sizeof(float)];
    } v;
    for (int i = 0; i < sizeof(float); ++i) {
        v.uc[i] = (unsigned char)sqlite3_value_int(argv[i]);
    }
    sqlite3_result_double(ctx, v.f);
}

void binary_FLOAT2BYTES(sqlite3_context* ctx, int argc, sqlite3_value* argv[]) {
    union {
        float f;
        unsigned char bytes[4];
    } v;
    v.f = sqlite3_value_double(argv[0]);
    int bytenumber = sqlite3_value_int(argv[1]);
    sqlite3_result_int(ctx, v.bytes[bytenumber]);
}

//-----

void myhalpers_UINT16TOFLOAT(sqlite3_context* ctx, int argc, sqlite3_value* argv[]) {
    union {
        float f;
        unsigned short us[sizeof(float)/sizeof(short)];
    } v;
    for (int i = 0; i < sizeof(float)/sizeof(short); ++i) {
        v.us[i] = (unsigned short)sqlite3_value_int(argv[i]);
    }
    sqlite3_result_double(ctx, v.f);
}

void myhalpers_FLOAT2UINT16(sqlite3_context* ctx, int argc, sqlite3_value* argv[]) {
    union {
        float f;
        unsigned short us[sizeof(float)/sizeof(short)];
    } v;
    v.f = sqlite3_value_double(argv[0]);
    int bytenumber = sqlite3_value_int(argv[1]);
    if (bytenumber > sizeof(float)/sizeof(short) - 1)
        sqlite3_result_error(ctx, "2nd argument mast be 0 or 1", 0);
    else
        sqlite3_result_int(ctx, v.us[bytenumber]);
}

//-----


//----


#ifdef _WIN32
__declspec(dllexport)
#endif
/* TODO: Change the entry point name so that "extension" is replaced by
** text derived from the shared library filename as follows:  Copy every
** ASCII alphabetic character from the filename after the last "/" through
** the next following ".", converting each character to lowercase, and
** discarding the first three characters if they are "lib".
*/
int sqlite3_extension_init(
        sqlite3 *db,
        char **pzErrMsg,
        const sqlite3_api_routines *pApi
        ){
    int rc = SQLITE_OK;
    SQLITE_EXTENSION_INIT2(pApi);
    sqlite3_create_function(
                db, // db context
                "binary_INT2FLOAT", // function name
                4, // количество аргументов
                0, //  SQLITE_DETERMINISTIC
                0, // user data
                binary_INT2FLOAT, // callback for scalar SQL function
                NULL,
                NULL
                );
    sqlite3_create_function(
                db, // db context
                "binary_FLOAT2BYTES", // function name
                2, // количество аргументов
                0, //  SQLITE_DETERMINISTIC
                0, // user data
                binary_FLOAT2BYTES, // callback for scalar SQL function
                NULL,
                NULL
                );
    sqlite3_create_function(
                db, // db context
                "myhalpers_UINT16TOFLOAT", // function name
                2, // количество аргументов
                0, //  SQLITE_DETERMINISTIC
                0, // user data
                myhalpers_UINT16TOFLOAT, // callback for scalar SQL function
                NULL,
                NULL
                );
    sqlite3_create_function(
                db, // db context
                "myhalpers_FLOAT2UINT16", // function name
                2, // количество аргументов
                0, //  SQLITE_DETERMINISTIC
                0, // user data
                myhalpers_FLOAT2UINT16, // callback for scalar SQL function
                NULL,
                NULL
                );

    return rc;
}
