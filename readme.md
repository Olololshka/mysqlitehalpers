mySQLiteHalpers
===

## About
A small pluguin for SQLite on pure C, implements some binary-transform functions

## Building
1. Clone this repo
2. Create **build** subdirectory any place you want
2. Launch **[CMake](https://cmake.org)** and set the path to clonned directory mySQLiteHalpers
3. Run **make**
4. Result library will be named **libmySQLiteHalpers**
5. Load extantion in your application using function **sqlite3_load_extension()**
6. For SQLite command-line interface use command .load /path/to/mySQLiteHalpers

## Set of SQL functions
binary_INT2FLOAT(b0, b1, b2, b3) == (b0 | (b1 << 8) | (b2 << 16) | (b3 << 24)) as float
binary_FLOAT2BYTES(f, bytenum) == (float as uint32_t)(f) -> get byte bytenum

myhalpers_UINT16TOFLOAT(s0, s1) == (s0 | (s1 << 16)) as float
myhalpers_FLOAT2UINT16(f, shortnum) == (float as uint32_t)(f) -> get short shortnum


Copyright (c) 2017, ololoshka2871
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

